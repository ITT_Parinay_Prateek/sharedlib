def call(def bName , def url, def cred) {
    //git branch: bName, credentialsId: 'Bit-buck', url: 'https://ITT_Parinay_Prateek@bitbucket.org/ITT_Parinay_Prateek/jenkins.git'
    git branch: bName, credentialsId: cred, url: url
}
